/**
 * the ut for jsdoc about param
 *
 */
export namespace test {
  /**
   * @param { string } str - The str description.
   * @param { string } str2 - The str2 description.
   */
  function func(str: string, str2: string): void;
}
