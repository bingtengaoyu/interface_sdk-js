declare namespace Test {
  export enum Valuable {
    VALUE_ONE,
    VALUE_Two = 'testTwo',
    VALUE_THREE = 'testThree',
  }
}
