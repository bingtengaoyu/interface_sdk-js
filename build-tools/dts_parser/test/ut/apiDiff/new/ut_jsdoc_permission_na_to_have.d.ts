/**
 * the ut for jsdoc about add permission
 *
 */
export namespace test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS
   */
  function func(str: string): void;
}
