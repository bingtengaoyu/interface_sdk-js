/**
 * the ut for jsdoc about change permission range to smaller
 *
 */
export namespace test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS
   */
  function func(str: string): void;
}
