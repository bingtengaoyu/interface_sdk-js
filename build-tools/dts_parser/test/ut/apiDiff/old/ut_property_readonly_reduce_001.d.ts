/**
 * the ut for property about reduce one which is readonly
 *
 */
export interface Test {
  readonly constant: 1;
  readonly constant2: 2;
}
