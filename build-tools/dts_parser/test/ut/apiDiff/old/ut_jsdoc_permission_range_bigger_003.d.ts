/**
 * the ut for jsdoc about change permission range to bigger
 *
 */
export namespace test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS and ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  function func(str: string): void;
}
