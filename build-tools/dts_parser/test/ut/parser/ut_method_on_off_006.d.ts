/**
 * the ut for method of on or off
 *
 */
export interface Test {
  on(callback: AsyncCallback<T>): void;
}
