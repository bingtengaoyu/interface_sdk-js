/**
 * the ut for jsdoc about extends
 *
 * @extends Array<string>
 */
 export interface Test extends Array<string> {}
