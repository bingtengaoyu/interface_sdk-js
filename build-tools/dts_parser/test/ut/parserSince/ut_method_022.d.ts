/**
 * the ut for method in class, which doesn't have params, but has return value
 */
export class Test {
  test(): number;
}